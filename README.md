# RSFM

A File Manager made in Rust/RS

RSFM Means: Rust File Manager

##### Disclaimer
This is a VERY WIP Project, and it's the first project on Rust that i've made.

# LISENCE
This project is lisenced with [WTFPL](http://www.wtfpl.net/)

# RUNNING THE PROGRAM
Run this commands. You'll need Git installed.

```
git clone https://gitlab.com/nitsuga5124/rsfm
cd rsfm
cargo run
```
```
$ git clone https://gitlab.com/nitsuga5124/rsfm
$ cd rsfm
$ cargo run
```

Cargo is obtained from [The official Rust Lang website](https://www.rust-lang.org/)
