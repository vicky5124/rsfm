extern crate gio; // gets GIO from a cargo crate
extern crate gtk; // gets GTK from a cargo crate
extern crate rand; // gets RAND from a cargo crate

use gio::prelude::*; // includes GIO
use gtk::prelude::*; // includes GTK
use rand::Rng; // includes random number generation
use gtk::{
    CellRendererText,TreeStore, TreeView, TreeViewColumn,
};

use std::env::args; // includes from the standard enviroment ARGS (Arguments)

//let mut number = 0;
fn counter(){
    //number++;
    let mut rng = rand::thread_rng(); // creates an rng thread
    let number = rng.gen::<u16>(); // generates a random u (unsigned int) or i (signed int) of 16 bits
    println!("{}", &number); // prints the randomly generated number
}

fn append_text_column(tree: &TreeView) {
    let column = TreeViewColumn::new();
    let cell = CellRendererText::new();

    column.pack_start(&cell, true);
    column.add_attribute(&cell, "text", 0);
    tree.append_column(&column);
}

fn build_ui(application: &gtk::Application) { // function that builds the window
    let window = gtk::ApplicationWindow::new(application); // Creates the main window

    window.set_title("First GTK+ Program"); // sets the title of the window
    window.set_border_width(10); // sets the border width to the window
    window.set_position(gtk::WindowPosition::Center); // makes it appear on the center
    // window.set_default_size(350, 70); // sets the default size, this doesn't make it unmodifyable

    let left_tree = TreeView::new(); // creates a tree view
    let left_store = TreeStore::new(&[String::static_type()]); // creates a model the left tree will follow

    left_tree.set_model(Some(&left_store)); // applies the model to the tree view variable
    left_tree.set_headers_visible(false); // disables the header
    append_text_column(&left_tree);

    for i in 0..10 { // a loop that only runs 10 times
        // insert_with_values takes two slices: column indices and ToValue
        // trait objects. ToValue is implemented for strings, numeric types,
        // bool and Object descendants
        let iter = left_store.insert_with_values(None, None, &[0], &[&format!("Hello {}", i)]);

        left_store.insert_with_values(Some(&iter), None, &[0], &[&"Test child node"]);
    }

    

    let button = gtk::Button::new_with_label("Click me!"); // sets a variable to be a button
    &button.connect_clicked(move |_| { // detects a button press; runs the following code as lambda
        counter(); // runs this function when the button is pressed
    });

    &left_tree.connect_row_activated(move |_| { 
        counter(); 
    });

    //window.add(&button); // adds the button to the window
    window.add(&left_tree); // adds the left tree to the window

    window.show_all(); // creates the window
}

fn main() { // Main function, duh
    let application = // creates an application variable
        gtk::Application::new(Some("com.nitsuga.basic_gui.basic"), // creates a new application
        Default::default()) // with default flags
            .expect("Initialization failed..."); // calls an exception if it fails to execute fsr

    application.connect_activate(|app| { // creates the main window
        build_ui(app); // runs the build_ui function
    });

    application.run(&args().collect::<Vec<_>>()); // runs the GUI Application
}
